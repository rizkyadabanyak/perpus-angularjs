
var logoutCtrl =  function($scope,$http,$window,$location,$rootScope){
    var getReq = {
        method: 'POST',
        url: 'http://localhost:8000/api/v1/client/logout',
        headers : {
            'Content-Type' : 'application/json',
            Authorization : "Bearer " + $window.localStorage.getItem('token')
        }   
    };
    $http(getReq).then(function (response){
        console.log(response.data)
        $window.localStorage.clear();
        $location.path('/books');
        $rootScope.cek = null;
        if(response.data.message = 'Unauthenticated.'){
            $location.path('/customers/auth/login');
        }
    },function (error){
        $location.path('/customers/auth/login');
        console.log(error.data)
    });
}

var myController = function($scope,$http,$window,$location,$state,$rootScope){
    
    $scope.pesan = function(id,quantity,date){
        var getReq = {
            method: 'POST',
            url: 'http://localhost:8000/api/v1/client/operate/cart',
            data: {  
                book_id: id,
                quantity: quantity,
                end_id: date
            },
            headers : {
                'Content-Type' : 'application/json',
                Authorization : "Bearer " + $window.localStorage.getItem('token')
            }
        };
        $http(getReq).then(function (response){
            $scope.loaded = true;
            console.log(response.data)
            alert(response.data.msg)
        },function (error){
            alert(error.data);
            $scope.loaded = true;  
        });
    }
  
    $scope.booksDetail = function(slug){
        $state.go("booksDetail", { Slug: slug });
    }

    var getReq = {
        method: 'GET',
        url: 'http://localhost:8000/api/v1/client/operate/cekProfile',
        headers : {
            'Content-Type' : 'application/json',
            Authorization : "Bearer " + $window.localStorage.getItem('token')
        }
    };
    $http(getReq).then(function (response){
        $scope.loaded = true;
        $rootScope.cek = 'auth';

    },function (error){
        $scope.loaded = true;
        $rootScope.cek = null;
    });

    $http.get('http://localhost:8000/api/v1/categories').then(function(res){
        $scope.categories = res.data;
    })
}



var booksCtrl = function($scope,$http){
    
    $http.get('http://localhost:8000/api/v1/books').then(function(res){

        $scope.loaded = true;
        $scope.books = res.data;
        
    })
}




var historiesCtrl = function($scope,$http,$window){
    
    var getReq = {
        method: 'GET',
        url: 'http://localhost:8000/api/v1/client/operate/histories',
        headers : {
            'Content-Type' : 'application/json',
            Authorization : "Bearer " + $window.localStorage.getItem('token')
        }
    };  
    $http(getReq).then(function (response){
        $scope.loaded = true;
        $scope.histories = response.data;
        console.log(response.data);
    },function (error){
        console.log(error.data);
        $scope.loaded = true;
    });
}

var cartsCtrl = function($scope,$http,$window){

    var getReq = {
        method: 'GET',
        url: 'http://localhost:8000/api/v1/client/operate/cart',
        headers : {
            'Content-Type' : 'application/json',
            Authorization : "Bearer " + $window.localStorage.getItem('token')
        }
    };  
    var iniCart = function(){
        $http(getReq).then(function (response){
            $scope.loaded = true;
            $scope.carts = response.data;
    
            console.log(response.data);
    
        },function (error){
            console.log(error.data);
            $scope.loaded = true;
    
        });
    }  
    iniCart()
    $scope.cekout = function(){
        var getReq = {
            method: 'POST',
            url: 'http://localhost:8000/api/v1/client/operate/rent',
            headers : {
                'Content-Type' : 'application/json',
                Authorization : "Bearer " + $window.localStorage.getItem('token')
            }
        };
        $http(getReq).then(function (response){
            $scope.loaded = true;
            console.log(response.data)
            alert(response.data.msg)
            iniCart()
            
        },function (error){
            alert(error.data);
            $scope.loaded = true;  
        });
        }
    }



var booksDetailCtrl = function($scope,$http,$stateParams){

    $http.get('http://localhost:8000/api/v1/date/').then(function(res){

        $scope.loaded = true;
        $scope.dates = res.data;
        console.log(res.data);

        
    })
    // console.log($stateParams)
    $http.get('http://localhost:8000/api/v1/books/'+$stateParams.Slug).then(function(res){

        $scope.loaded = true;
        $scope.book = res.data;
        console.log(res.data);
        angular.extend($scope, res.data);

        
    })

}

var returnBooksCtrl = function($scope,$http,$window){
    $scope.submit = function(){
        var postReq = {
            method: 'POST',
            url: 'http://localhost:8000/api/v1/client/operate/return',
            data: {  
                code: $scope.code,
            },
            headers : {
                'Content-Type' : 'application/json',
                Authorization : "Bearer " + $window.localStorage.getItem('token')
            }
        };
        $http(postReq).then(function (response){
            console.log(response.data)
            $scope.books = response.data;
        },function (error){
            console.log(error.data)

        });
        
    }
}

//ini controller login
var loginCtrl = function($scope,$http,$window,$location,$rootScope){
    var getReq = {
        method: 'GET',
        url: 'http://localhost:8000/api/v1/client/operate/cekProfile',
        headers : {
            'Content-Type' : 'application/json',
            Authorization : "Bearer " + $window.localStorage.getItem('token')
        }
    };

    $http(getReq).then(function (response){
        $scope.loaded = true;
        if(response.data.message = 'Unauthenticated.'){
            $location.path('/customers/auth/login');
        }
        $location.path('/books');
    },function (error){
        console.log(error.data);
        $scope.loaded = true;
        $location.path('/customers/auth/login');

    });

    $scope.submit = function(){
        var postReq = {
            method: 'POST',
            url: 'http://localhost:8000/api/v1/client/login',
            data: {  
                email: $scope.email,
                password: $scope.password
            },
            headers : {
                'Content-Type': 'application/json'
            }
        };
        $http(postReq).then(function (response){
            if(response.data.status == 'danger'){
                alert('ada kesalahan')
            }else{
                $auth = 'auth'
                $rootScope.cek = 'auth';
                console.log($auth);
                console.log(response.data)
                $window.localStorage.setItem('token', response.data.token)
                $location.path('/books');
            }
           
            

        },function (error){
            console.log(error.data)

        });
        
    }
}

var regisCtrl =  function($scope,$http,$window,$location){
    var getReq = {
        method: 'GET',
        url: 'http://localhost:8000/api/v1/client/operate/cekProfile',
        headers : {
            'Content-Type' : 'application/json',
            Authorization : "Bearer " + $window.localStorage.getItem('token')
        }
    };
    $http(getReq).then(function (response){
        $scope.loaded = true;
        $location.path('/books');
    },function (error){
        $scope.loaded = true;
        $location.path('/customers/auth/register');
        console.log(error.data)
    });
    
    $scope.submit = function(){
        var postReq = {
            method: 'POST',
            url: 'http://localhost:8000/api/v1/client/regis',
            data: {  
                name: $scope.name,
                email: $scope.email,
                password: $scope.password
            },
            headers : {
                'Content-Type': 'application/json'
            }
        };
        $http(postReq).then(function (response){
            console.log(response.data)
            $window.localStorage.setItem('token', response.data.token)
            $location.path('/books');
            

        },function (error){
            console.log(error.data)

        });
        
    }
}
var bookAdminCtrl = function($scope,$http,$window,$location,$rootScope){

    var getReq = {
        method: 'GET',
        url: 'http://localhost:8000/api/v1/auth/admin/operate/cekProfileAdmin',
        headers : {
            'Content-Type' : 'application/json',
            Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
        }
    };

    $http(getReq).then(function (response){
        $scope.loaded = true;
        if(response.data.message = 'Unauthenticated.'){
            $location.path('/admin/ganteng/login');
        }
        $location.path('/admin/ganteng/home');
    },function (error){
        console.log(error.data);
        $scope.loaded = true;   
        $location.path('/admin/ganteng/login');
    });


    $http.get('http://localhost:8000/api/v1/books').then(function(res){

        $scope.loaded = true;
        $scope.books = res.data;
        console.log(res.data);
    })


    $scope.logout = function(){
        var getReq = {
            method: 'POST',
            url: 'http://localhost:8000/api/v1/auth/admin/logout',
            headers : {
                'Content-Type' : 'application/json',
                Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
            }   
        };
        $http(getReq).then(function (response){
            $window.localStorage.removeItem('token-admin');
            $location.path('/books');
            if(response.data.message = 'Unauthenticated.'){
                $location.path('/admin/ganteng/login');
            }
        },function (error){
            $location.path('/admin/ganteng/login');
            console.log(error.data)
        });
    
    }
    $http.get('http://localhost:8000/api/v1/categories').then(function(res){
        $scope.categories = res.data;
    })

    $scope.selectBook = function(s){
        $scope.form = angular.copy(s);

    }

    var file = $scope.uploadedFile = function(element) {     
    
        $scope.$apply(function($scope) {
            $scope.files = element.files;  
            // console.log(element.files)
        });
    
    }
    $scope.submit = function(name,category_id,year,author,publisher,dsc,stock){
        var s = angular.copy($scope.form);
        var url = "http://localhost:8000/api/v1/auth/admin/operate/book";
        // console.log(s.id)
    
        if(s.id){
            // $http.post(url,s).then(function (res){

            // })
            console.log('ini update')

        }else{
            console.log($scope.form.img)
            var fd = new FormData();
            var files = document.getElementById('file').files[0];
            fd.append('file',files);

            console.log(fd)
            var getReq = {
                method: 'POST',
                url: 'http://localhost:8000/api/v1/auth/admin/operate/book',
                data:
                //  $scope.form,
                 {  
                    name: name,
                    category_id: category_id,
                    year: year,
                    author: author,
                    publisher: publisher,
                    dsc: dsc,
                    stock: stock,
                    image : fd
                },
                headers : {
                    'Content-Type' : 'application/json',
                    Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
                }
            };
            $http(getReq).then(function (response){
                $scope.loaded = true;
                $http.get('http://localhost:8000/api/v1/books').then(function(res){

                    $scope.loaded = true;
                    $scope.books = res.data;
                    console.log(res.data.message);
                })            
            },function (error){
                console.log(error.data)
                alert(error.data.message);
                $scope.loaded = true;  
            });
            
        }
    }

}

var loginAdminCtrl = function($scope,$http,$window,$location,$rootScope){
    var getReq = {
        method: 'GET',
        url: 'http://localhost:8000/api/v1/auth/admin/operate/cekProfileAdmin',
        headers : {
            'Content-Type' : 'application/json',
            Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
        }
    };

    $http(getReq).then(function (response){
        $scope.loaded = true;
        if(response.data.message = 'Unauthenticated.'){
            $location.path('/admin/ganteng/login');
        }
        $location.path('/admin/ganteng/home');
    },function (error){
        console.log(error.data);
        $scope.loaded = true;   
        $location.path('/admin/ganteng/login');
    });

    $scope.submit = function(){
        var postReq = {
            method: 'POST',
            url: 'http://localhost:8000/api/v1/auth/admin/login',
            data: {  
                email: $scope.email,
                password: $scope.password
            },
            headers : {
                'Content-Type': 'application/json'
            }
        };
        $http(postReq)
        .then(function (response){
            if(response.data.status == 'danger'){
                alert('ada kesalahan')
            }else{
                $window.localStorage.setItem('token-admin', response.data.token)
                $location.path('/admin/ganteng/home');
            }
           
        },function (error){
            console.log(error.data)

        });
        
    }
}

var regisAdminCtrl =  function($scope,$http,$window,$location,$rootScope){
    
}

var categoryAdminCtrl =  function($scope,$http,$window,$location,$rootScope){
    $http.get('http://localhost:8000/api/v1/categories').then(function(res){
        $scope.categories = res.data;
    })
    $scope.select = function(s){
        $scope.form = angular.copy(s);

    }
    $scope.delete = function(){
        var id = $scope.form.id;
        if(id){
            var getReq = {
                method: 'DELETE',
                url: 'http://localhost:8000/api/v1/auth/admin/operate/category/'+id,
                headers : {
                    'Content-Type' : 'application/json',
                    Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
                }
            };
            $http(getReq).then(function (response){
                $scope.loaded = true;            
                $http.get('http://localhost:8000/api/v1/categories').then(function(res){
                    $scope.loaded = true;
                    $scope.categories = res.data;
                })
            },function (error){
                $scope.loaded = true;  
            });
        }
    }

    $scope.submit = function(){
        var s = angular.copy($scope.form);
    
        if(s.id){
            var getReq = {
                method: 'PUT',
                url: 'http://localhost:8000/api/v1/auth/admin/operate/category/'+s.id,
                data: s,
                headers : {
                    'Content-Type' : 'application/json',
                    Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
                }
            };
            $http(getReq).then(function (response){
                $scope.loaded = true;            
                $http.get('http://localhost:8000/api/v1/categories').then(function(res){
                    $scope.loaded = true;
                    $scope.categories = res.data;
                })
            },function (error){
                console.log(error.data)
                alert(error.data.message);
                $scope.loaded = true;  
            });

        }else{
        
            var getReq = {
                method: 'POST',
                url: 'http://localhost:8000/api/v1/auth/admin/operate/category',
                data: s,
                headers : {
                    'Content-Type' : 'application/json',
                    Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
                }
            };
            $http(getReq).then(function (response){
                $scope.loaded = true;            
                $http.get('http://localhost:8000/api/v1/categories').then(function(res){
                    $scope.loaded = true;
                    $scope.categories = res.data;
                })
            },function (error){
                console.log(error.data)
                alert(error.data.message);
                $scope.loaded = true;  
            });
            
        }
        
    }
    

}

var endDateAdminCtrl =  function($scope,$http,$window,$location,$rootScope){
    $http.get('http://localhost:8000/api/v1/date/').then(function(res){

        $scope.loaded = true;
        $scope.dates = res.data;
        console.log(res.data);

        
    })
    $scope.select = function(s){
        $scope.form = angular.copy(s);

    }
    $scope.delete = function(){
        var id = $scope.form.id;
        if(id){
            var getReq = {
                method: 'DELETE',
                url: 'http://localhost:8000/api/v1/auth/admin/operate/end_date/'+id,
                headers : {
                    'Content-Type' : 'application/json',
                    Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
                }
            };
            $http(getReq).then(function (response){
                $scope.loaded = true;            
                $http.get('http://localhost:8000/api/v1/date/').then(function(res){

                    $scope.loaded = true;
                    $scope.dates = res.data;
                
                })
            },function (error){
                $scope.loaded = true;  
            });
        }
    }

    $scope.submit = function(){
        var s = angular.copy($scope.form);
        if(s.id){
            var getReq = {
                method: 'PUT',
                url: 'http://localhost:8000/api/v1/auth/admin/operate/end_date/'+s.id,
                data: s,
                headers : {
                    'Content-Type' : 'application/json',
                    Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
                }
            };
            $http(getReq).then(function (response){
                $scope.loaded = true;            
                $http.get('http://localhost:8000/api/v1/date/').then(function(res){

                    $scope.loaded = true;
                    $scope.dates = res.data;
                
                })
            },function (error){
                console.log(error.data)
                alert(error.data.message);
                $scope.loaded = true;  
            });

        }else{
        
            var getReq = {
                method: 'POST',
                url: 'http://localhost:8000/api/v1/auth/admin/operate/end_date',
                data: s,
                headers : {
                    'Content-Type' : 'application/json',
                    Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
                }
            };
            $http(getReq).then(function (response){
                $scope.loaded = true;            
                $http.get('http://localhost:8000/api/v1/date/').then(function(res){

                    $scope.loaded = true;
                    $scope.dates = res.data;
                
                })
            },function (error){
                console.log(error.data)
                alert(error.data.message);
                $scope.loaded = true;  
            });
            
        }
        
    }
}

var historyAdminCtrl =  function($scope,$http,$window,$location,$rootScope){
    var getReq = {
        method: 'GET',
        url: 'http://localhost:8000/api/v1/auth/admin/operate/histories',
        headers : {
            'Content-Type' : 'application/json',
            Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
        }
    };
    $http(getReq).then(function (response){
        $scope.loaded = true;            
        $scope.histories = response.data;
        console.log(response.data)

    },function (error){
        
        $scope.loaded = true;  
        console.log(error.data)

    });
}


var pengembalianAdminCtrl =  function($scope,$http,$window,$location,$rootScope){
    $scope.submit = function(){
        var postReq = {
            method: 'POST',
            url: 'http://localhost:8000/api/v1/auth/admin/operate/returnAdmin',
            data: {  
                code: $scope.form.code,
            },
            headers : {
                'Content-Type' : 'application/json',
                Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
            }
        };
        $http(postReq).then(function (response){
            // alert(response.data);
            console.log(response.data)
            $scope.books = response.data;
        },function (error){
            // console.log(error.data)
            // alert(error.data);

        });
        
    }

    $scope.kembalikan = function(code,book_id){
        var postReq = {
            method: 'POST',
            url: 'http://localhost:8000/api/v1/auth/admin/operate/actionReturn',
            data: {  
                code : code,
                book_id : book_id,
            },
            headers : {
                'Content-Type' : 'application/json',
                Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
            }
        };
        $http(postReq).then(function (response){
            alert(response.data.msg);

            var postReq = {
                method: 'POST',
                url: 'http://localhost:8000/api/v1/auth/admin/operate/returnAdmin',
                data: {  
                    code: code,
                },
                headers : {
                    'Content-Type' : 'application/json',
                    Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
                }
            };
            $http(postReq).then(function (response){
                // alert(response.data);
                console.log(response.data)
                $scope.books = response.data;
            },function (error){
                // console.log(error.data)
                // alert(error.data);
    
            });
        },function (error){
            // console.log(error.data)
            alert(error.data);

        });
        
    }
}

var pengambilanAdminCtrl =  function($scope,$http,$window,$location,$rootScope){
    $scope.submit = function(){
        var postReq = {
            method: 'POST',
            url: 'http://localhost:8000/api/v1/auth/admin/operate/getPengambilan',
            data: {  
                code: $scope.form.code,
            },
            headers : {
                'Content-Type' : 'application/json',
                Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
            }
        };
        $http(postReq).then(function (response){
            // alert(response.data.code);
            $scope.codes = response.data;
        },function (error){
            // console.log(error.data)
            // alert(error.data);

        });
        
    }

    $scope.ambil = function(code){
        var postReq = {
            method: 'POST',
            url: 'http://localhost:8000/api/v1/auth/admin/operate/actionPengambilan',
            data: {  
                code: $scope.form.code,
            },
            headers : {
                'Content-Type' : 'application/json',
                Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
            }
        };
        $http(postReq).then(function (response){
            var postReq = {
                method: 'POST',
                url: 'http://localhost:8000/api/v1/auth/admin/operate/getPengambilan',
                data: {  
                    code: code,
                },
                headers : {
                    'Content-Type' : 'application/json',
                    Authorization : "Bearer " + $window.localStorage.getItem('token-admin')
                }
            };
            $http(postReq).then(function (response){
                // alert(response.data.code);
                $scope.codes = response.data;
            },function (error){
                // console.log(error.data)
                // alert(error.data);
    
            });
        },function (error){
            // console.log(error.data)
            // alert(error.data);

        });
        
    }

}
var perpusConfig = function($stateProvider, $urlRouterProvider) {

var myState = {
    home: {
        name : 'home',
        url: '/',
        template : '<h1>ini home</h1>'
    },
    books: {
        name : 'books',
        url: '/books',
        templateUrl : 'client/books.html',
        controller:'books-ctrl'
    },
    carts: {
        name : 'carts',
        url: '/carts',
        templateUrl : 'client/cart.html',
        controller:'carts-ctrl'
    },
    histories: {
        name : 'histories',
        url: '/histories',
        templateUrl : 'client/histories.html',
        controller:'histories-ctrl'
    },
    booksDetail: {
        name : 'booksDetail',
        url: '/books/:Slug',
        templateUrl : 'client/booksDetail.html',
        controller:'booksDetail-ctrl'
    },
    returnBooks: {
        name : 'returnBooks',
        url: '/return-books',
        templateUrl : 'client/pengembalian.html',
        controller:'returnBooks-ctrl'
    },
    login: {
        name : 'login',
        url: '/customers/auth/login',
        templateUrl : 'client/auths/login.html',
        controller:'loginCustomer-ctrl'
    },
    register: {
        name : 'register',
        url: '/customers/auth/register',
        templateUrl : 'client/auths/regis.html',
        controller:'registerCustomer-ctrl'
    },
    logout : {
        name : 'logout',
        url: '/customers/auth/logout',
        controller:'logoutCustomer-ctrl'
    }
};

var stateAdmin = {
    homeAdmin: {
        name : 'homeAdmin',
        url: '/admin/ganteng/home',
        templateUrl : 'admin/templates/book.html',
        controller:'bookAdmin-ctrl' 
       },
    loginAdmin: {
        name : 'loginAdmin',
        url: '/admin/ganteng/login',
        templateUrl : 'admin/auths/login.html',
        controller:'loginAdmin-ctrl'
    },
    regisAdmin: {
        name : 'regisAdmin',
        url: '/admin/ganteng/register',
        templateUrl : 'admin/auths/regis.html',
        controller:'regisAdmin-ctrl'
    },
    categoryAdmin: {
        name : 'categoryAdmin',
        url: '/admin/ganteng/category',
        templateUrl : 'admin/templates/category.html',
        controller:'categoryAdmin-ctrl' 
    },
    endDateAdmin: {
        name : 'endDateAdmin',
        url: '/admin/ganteng/endDate',
        templateUrl : 'admin/templates/endDate.html',
        controller:'endDateAdmin-ctrl' 
    },
    historyAdmin: {
        name : 'historyAdmin',
        url: '/admin/ganteng/history',
        templateUrl : 'admin/templates/history.html',
        controller:'historyAdmin-ctrl' 
    },
    pengembalianAdmin: {
        name : 'pengembalianAdmin',
        url: '/admin/ganteng/inputCode',
        templateUrl : 'admin/templates/pengembalian.html',
        controller:'pengembalianAdmin-ctrl' 
    },
    pengambilanAdmin: {
        name : 'pengambilanAdmin',
        url: '/admin/ganteng/pengambilanBuku',
        templateUrl : 'admin/templates/pengambilan.html',
        controller:'pengambilanAdmin-ctrl' 
    },
       
};
    $stateProvider
    .state(myState.home)
    .state(myState.books)
    .state(myState.carts)
    .state(myState.booksDetail)
    .state(myState.histories)
    .state(myState.returnBooks)
    .state(myState.login)
    .state(myState.register)
    .state(myState.logout)
    .state(stateAdmin.homeAdmin)
    .state(stateAdmin.loginAdmin)
    .state(stateAdmin.regisAdmin)
    .state(stateAdmin.categoryAdmin)
    .state(stateAdmin.historyAdmin)
    .state(stateAdmin.endDateAdmin)
    .state(stateAdmin.pengembalianAdmin)
    .state(stateAdmin.pengambilanAdmin)

    // $urlRouterProvider.otherwise("/");
    
}
angular.module('xapp',["ui.router"])
.config(perpusConfig)
.controller('xctrl',myController)
.controller('books-ctrl',booksCtrl)
.controller('carts-ctrl',cartsCtrl)
.controller('histories-ctrl',historiesCtrl)
.controller('booksDetail-ctrl',booksDetailCtrl)
.controller('returnBooks-ctrl',returnBooksCtrl)
.controller('loginCustomer-ctrl',loginCtrl)
.controller('registerCustomer-ctrl',regisCtrl)
.controller('logoutCustomer-ctrl',logoutCtrl)
.controller('bookAdmin-ctrl',bookAdminCtrl)
.controller('loginAdmin-ctrl',loginAdminCtrl)
.controller('regisAdmin-ctrl',regisAdminCtrl)
.controller('categoryAdmin-ctrl',categoryAdminCtrl)
.controller('endDateAdmin-ctrl',endDateAdminCtrl)
.controller('historyAdmin-ctrl',historyAdminCtrl)
.controller('pengembalianAdmin-ctrl',pengembalianAdminCtrl)
.controller('pengambilanAdmin-ctrl',pengambilanAdminCtrl);